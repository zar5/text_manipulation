def remove_nextline():
    with open('example_text.txt') as f:
        all = f.readlines()
        NON = ''
        for line in all:
            NON = NON + ' ' + line.rstrip('\n')
        return NON

def count_men():
    with open('example_text.txt') as f:
        counter = 0
        all = remove_nextline()
        lowerline = all.lower()
        words = lowerline.rsplit(' ')
        for w in words:
            s = w.lower()
            if w[0:3] == 'men':
                if len(w) == 3:
                    counter += 1
                elif len(w) > 3 and (w[3] == '.' or w[4] == 's'):
                    print(w[3])
                    counter += 1
            else:
                pass
            continue
        print(counter)


if __name__ == "__main__":
    remove_nextline()
    count_men()